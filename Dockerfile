# Specify parent image
ARG BASE_IMAGE=registry.git.rwth-aachen.de/acs/cloud/jupyter/singleuser/python:latest
FROM ${BASE_IMAGE}

# Update conda base environment to match specifications in environment.yml
ADD environment.yml /tmp/environment.yml
USER root
RUN sed -i "s|name\: jupyter-quickstart|name\: base|g" /tmp/environment.yml # we need to replace the name of the environment with base such that we can update the base environment here
USER $NB_USER
RUN conda env update -f /tmp/environment.yml # All packages specified in environment.yml are installed in the base environment

# Cleanup conda packages
RUN conda clean --all -f -y

# Execute postBuild script
ADD binder/postBuild /tmp/postBuild.sh
USER root
RUN chmod +x /tmp/postBuild.sh # Make the file executable (TODO: is this still needed?)
USER $NB_USER
RUN /tmp/postBuild.sh

# Copy workspace
COPY ./ /home/jovyan
